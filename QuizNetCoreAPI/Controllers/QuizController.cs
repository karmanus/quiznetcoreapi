﻿using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ApplicationCore.Interfaces;
using QuizNetCoreAPI.ViewModel;
using AutoMapper;

namespace QuizNetCoreAPI.Controllers
{
    [ApiController]
    [Route("quiz")]
    public class QuizController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IQuizMapper _quizGetter;

        public QuizController(IMapper mapper, IQuizMapper quizGetter)
        {
            _mapper = mapper;
            _quizGetter = quizGetter;
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        [Route("getquiz")]
        public QuizViewModel GetQuiz(int id)
        {
            var quiz = _quizGetter.GetQuiz(id);
            return _mapper.Map<QuizViewModel>(quiz);
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        [Route("getquizes")]
        public IEnumerable<QuizViewModel> GetQuizes()
        {
            var quizes = _quizGetter.GetQuizes();
            var viewQuizes = quizes.Select(q => _mapper.Map<QuizViewModel>(q));
            return viewQuizes;
        }

        [HttpGet]
        [Authorize(Roles = "User")]
        [Route("getavquizes")]
        public IEnumerable<QuizViewModel> GetAvailebleQuizes()
        {
            var quizes = _quizGetter.GetAvailebleQuizes();
            var viewQuizes = quizes.Select(q => _mapper.Map<QuizViewModel>(q));
            return viewQuizes;
        }
    }
}
