﻿using System;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using ApplicationCore.Interfaces;
using QuizNetCoreAPI.ViewModel;
using ApplicationCore.Models;
using QuizNetCoreAPI.Interfaces;
using AutoMapper;
using QuizNetCoreAPI.Middlewares.Exceptions;

namespace QuizNetCoreAPI.Controllers
{
    [Authorize(Roles = "Admin")]
    [ApiController]
    [Route("editor")]
    public class QuizEditorController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IQuizEditor _editor;
        private readonly IQuizValidator _validator;

        public QuizEditorController(IMapper mapper, IQuizEditor editor, IQuizValidator validator)
        {
            _mapper = mapper;
            _editor = editor;
            _validator = validator;
        }

        [HttpPost]
        [Route("create")]
        public int Create(QuizViewModel quiz)
        {
            bool isQuizValid = _validator.ValidateQuiz(quiz);

            if (isQuizValid)
            {
                return _editor.CreateQuiz(_mapper.Map<QuizModel>(quiz));
            }

            throw new InvalidModelException("quiz");
        }

        [HttpDelete]
        [Route("delete")]
        public int Delete(int id)
        {
            return _editor.DeleteQuiz(id);
        }

        [HttpPut]
        [Route("answertext")]
        public int ChangeAnswerText(int answerId, string answerText)
        {            
            return _editor.ChangeAnswerText(answerId, answerText);
        }

        [HttpPut]
        [Route("questiontext")]
        public int ChangeQuestionText(int questionId, string questionText)
        {
            return _editor.ChangeQuestionText(questionId, questionText);
        }


        [HttpPost]
        [Route("addanswer")]
        public int AddAnswer(AnswerViewModel answer)
        {            
            return _editor.AddAnswer(_mapper.Map<AnswerModel>(answer));
        }

        [HttpDelete]
        [Route("deleteanswer")]
        public int DeleteAnswer(int id)
        {
            return _editor.DeleteAnswer(id);
        }

        [HttpDelete]
        [Route("deletetask")]
        public int DeleteTask(int id)
        {
           return _editor.DeleteTask(id);
        }

        [HttpPost]
        [Route("addtask")]
        public int AddQuizTask(TaskViewModel task)
        {
            var isTaksValid = _validator.ValidateQuizTask(task);

            if (isTaksValid)
            {
                return _editor.AddQuizTask(_mapper.Map<TaskModel>(task));
            }

            throw new InvalidModelException("task");
        }
    }
}
