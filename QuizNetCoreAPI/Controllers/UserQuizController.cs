﻿using ApplicationCore.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using QuizNetCoreAPI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizNetCoreAPI.Controllers
{
    [Authorize(Roles = "User")]
    [ApiController]
    [Route("userquiz")]
    public class UserQuizController : Controller
    {
        private readonly IQuizRunner _quizRunner;
        private readonly IMapper _mapper;

        public UserQuizController(IQuizRunner quizRunner, IMapper mapper)
        {
            _quizRunner = quizRunner;
            _mapper = mapper;
        }

        [HttpPost]
        [Route("start")]
        public UserQuizViewModel StartQuiz(int id)
        {
            return _mapper.Map<UserQuizViewModel>(_quizRunner.StartQuiz(HttpContext.User.Identity.Name, id));
        }

        [HttpPut]
        [Route("finish")]
        public int FinishQuiz(int id)
        {
            return _quizRunner.FinishQuiz(id);
        }

        [HttpPut]
        [Route("selectanswer")]
        public int SelectAnswer(int answerId, int taskId, bool choise)
        {
            return _quizRunner.SelectAnswer(answerId, taskId, choise);
        }
    }
}
