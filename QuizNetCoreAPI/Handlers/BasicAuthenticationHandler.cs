﻿using System;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using ApplicationCore.Interfaces;
using ApplicationCore.Models;

namespace QuizNetCoreAPI.Handlers
{
    /// <summary>
    ///     Handler for basic authorize. 
    /// </summary>
    public class BasicAuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        private IAuthorize authorizer;

        public BasicAuthenticationHandler(IOptionsMonitor<AuthenticationSchemeOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock,
            IAuthorize authorizer) : base(options, logger, encoder, clock)
        {
            this.authorizer = authorizer;
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            var header = Request.Headers;
            if (!Request.Headers.ContainsKey("Authorization"))
            {
                return AuthenticateResult.Fail("Authorization header was not found");
            }

            try
            {
                var authenticationHeaderValue = AuthenticationHeaderValue.Parse(Request.Headers["Authorization"]);
                var bytes = Convert.FromBase64String(authenticationHeaderValue.Parameter);
                string[] credentials = Encoding.UTF8.GetString(bytes).Split(":");
                string login = credentials[0];
                string password = credentials[1];

                UserModel user = UserModel.ConvertToUserModel(authorizer.Authorize(login, password));


                if (user == null)
                {
                    return AuthenticateResult.Fail("You didn't guess");
                }
                else
                {
                    var roleClaims = user.Roles.Select(r => new Claim(ClaimTypes.Role, r));
                    var claims = new[] { new Claim(ClaimTypes.Name, user.Name) }.ToList();
                    claims.AddRange(roleClaims);
                    var identity = new ClaimsIdentity(claims, Scheme.Name);
                    var principal = new ClaimsPrincipal(identity);
                    var ticket = new AuthenticationTicket(principal, Scheme.Name);

                    return AuthenticateResult.Success(ticket);
                }
            }
            catch (Exception ex)
            {
                string exMessage = ex.Message;
                return AuthenticateResult.Fail("Error has occured");
            }

        }
    }
}
