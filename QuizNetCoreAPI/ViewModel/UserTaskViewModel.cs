﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizNetCoreAPI.ViewModel
{
    public class UserTaskViewModel
    {
        public int Id { get; set; }
        public string Question { get; set; }
    }
}
