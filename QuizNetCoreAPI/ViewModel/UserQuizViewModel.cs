﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace QuizNetCoreAPI.ViewModel
{
    public class UserQuizViewModel
    {
        public int Id { get; set; }
        public int QuizId { get; set; }
        public string UserName { get; set; }
        public bool IsFinished { get; set; }
        public IEnumerable<UserTaskViewModel> Tasks { get; set; }
    }
}
