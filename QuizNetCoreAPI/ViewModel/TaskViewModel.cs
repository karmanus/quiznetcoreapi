﻿using System.Collections.Generic;

namespace QuizNetCoreAPI.ViewModel
{
    public class TaskViewModel
    {
        public int Id { get; set; }
        public bool IsMultipy { get; set; }        
        public string Question { get; set; }
        public int QuizId { get; set; }
        public IEnumerable<AnswerViewModel> Answers { get; set; }        
    }
}
