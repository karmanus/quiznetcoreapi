﻿namespace QuizNetCoreAPI.ViewModel
{
    public class UserAnswerViewModel
    {
        public int Id { get; set; }

        public string Text { get; set; }

        public bool IsSelected { get; set; }
    }
}
