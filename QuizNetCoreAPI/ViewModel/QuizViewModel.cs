﻿using System.Collections.Generic;

namespace QuizNetCoreAPI.ViewModel
{
    public class QuizViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsAvailable { get; set; }
        public IEnumerable<TaskViewModel> Tasks { get; set; }                
    }
}
