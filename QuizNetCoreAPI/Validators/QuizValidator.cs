﻿using QuizNetCoreAPI.Interfaces;
using QuizNetCoreAPI.ViewModel;
using System.Linq;

namespace QuizNetCoreAPI.Validators
{
    /// <inheritdoc/>
    public class QuizValidator : IQuizValidator
    {
        private readonly IQuizTaskValidator _taskValidator;
        private const int minTasksCount = 2;
        private const int maxTaskCount = 10;

        public QuizValidator(IQuizTaskValidator taskValidator)
        {
            _taskValidator = taskValidator;
        }
        public bool ValidateQuiz(QuizViewModel quiz)
        {
            return quiz != null && quiz.Tasks != null && quiz.Tasks.Count() >= minTasksCount && quiz.Tasks.Count() <= maxTaskCount && quiz.Tasks.All(t => _taskValidator.ValidateQuizTask(t));
        }

        public bool ValidateQuizTask(TaskViewModel task)
        {
            return _taskValidator.ValidateQuizTask(task);
        }
    }
}
