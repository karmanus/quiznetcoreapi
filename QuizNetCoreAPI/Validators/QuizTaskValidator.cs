﻿using QuizNetCoreAPI.Interfaces;
using QuizNetCoreAPI.ViewModel;
using System.Linq;

namespace QuizNetCoreAPI.Validators
{
    /// <inheritdoc/>
    public class QuizTaskValidator : IQuizTaskValidator
    {
        private const int maxAnswersCount = 5;
        private const int minAnswersCount = 2;         

        public bool ValidateQuizTask(TaskViewModel task)
        {
            return task != null && task.Answers != null && task.Answers.Count() <= maxAnswersCount && task.Answers.Count() >= minAnswersCount && task.Answers.Any(a => a.IsCorrect);
        }
    }
}
