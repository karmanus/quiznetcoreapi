using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using ApplicationCore;
using ApplicationCore.Convertors;
using ApplicationCore.Editors;
using ApplicationCore.Interfaces;
using ApplicationCore.Mappers;
using ApplicationCore.Models;
using QuizNetCoreAPI.DAL;
using QuizNetCoreAPI.Handlers;
using QuizNetCoreAPI.Interfaces;
using QuizNetCoreAPI.Validators;
using QuizNetCoreAPI.Middlewares;

namespace QuizNetCoreAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<ExceptionHandlingMiddleware>();
            services.AddControllers();
            services.AddAutoMapper(typeof(Startup));
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "QuizNetCoreAPI", Version = "v1" });
            });
            DataLayerServiceRegister.Configure(services);            
            services.AddScoped<IAuthorize, Authorizer>();
            services.AddScoped<IQuizEditor,QuizEditor>();
            services.AddScoped<IQuizMapper, QuizMapper>();
            services.AddScoped<IQuizTaskValidator, QuizTaskValidator>();
            services.AddScoped<IQuizValidator, QuizValidator>();
            services.AddScoped<IQuizConvertor, QuizConvertor>();
            services.AddScoped<IUserQuizConvertor, UserQuizConvertor>();
            services.AddScoped<IUserQuizesMapper, UserQuizeMapper>();
            services.AddScoped<IQuizRunner, QuizRunner>();
            services.AddAuthentication("BasicAuthentication")            
            .AddScheme<AuthenticationSchemeOptions, BasicAuthenticationHandler>("BasicAuthentication", null);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "QuizNetCoreAPI v1"));
            }

            app.UseRouting();

            app.UseMiddleware<ExceptionHandlingMiddleware>();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
