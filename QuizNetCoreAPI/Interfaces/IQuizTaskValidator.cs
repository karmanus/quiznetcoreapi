﻿using QuizNetCoreAPI.ViewModel;

namespace QuizNetCoreAPI.Interfaces
{
    /// <summary>
    /// For validate task view models
    /// </summary>
    public interface IQuizTaskValidator
    {
        /// <summary>
        ///     Validate quiz task.
        /// </summary>
        /// <param name="task"></param>
        /// <returns>Is valid</returns>
        public bool ValidateQuizTask(TaskViewModel task);
    }
}
