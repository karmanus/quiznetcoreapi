﻿using QuizNetCoreAPI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizNetCoreAPI.Interfaces
{
    /// <summary>
    ///     For quiz validating.
    /// </summary>
    public interface IQuizValidator : IQuizTaskValidator
    {
        /// <summary>
        ///     Validate quiz.
        /// </summary>
        /// <param name="quiz"></param>
        /// <returns>Is quiz valid</returns>
        public bool ValidateQuiz(QuizViewModel quiz);
    }
}
