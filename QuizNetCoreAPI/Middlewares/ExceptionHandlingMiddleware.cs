﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using QuizNetCoreAPI.Middlewares.Exceptions;

namespace QuizNetCoreAPI.Middlewares
{
    public class ExceptionHandlingMiddleware : IMiddleware
    {
        public async Task InvokeAsync(HttpContext context, RequestDelegate next)
        {
            try
            {
                await next(context);
            }
            catch(InvalidModelException ex)
            {
                await context.Response.WriteAsync("Invalid " + ex.ModelType + " model");
            }
            catch (Exception ex)
            {
                await context.Response.WriteAsync(ex.Message);
            }
        }
    }
}
