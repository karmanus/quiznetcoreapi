﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizNetCoreAPI.Middlewares.Exceptions
{
    public class InvalidModelException : Exception
    {
        public string ModelType { get; }

        public InvalidModelException(string modelType)
        {
            ModelType = modelType;
        }
    }
}
