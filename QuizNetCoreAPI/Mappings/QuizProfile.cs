﻿using ApplicationCore.Models;
using AutoMapper;
using QuizNetCoreAPI.ViewModel;
using System.Linq;

namespace QuizNetCoreAPI.Mappings
{
    /// <summary>
    ///     For Mapping quiz models and quiz view models.
    /// </summary>
    public class QuizProfile : Profile
    {
        public QuizProfile()
        {
            MapingQuizView();
            MapingQuiz();            
        }
        /// <summary>
        ///     Create map to view models.
        /// </summary>
        private void MapingQuizView()
        {
            CreateMap<QuizModel, QuizViewModel>()
                .ForMember(v => v.Id, c => c.MapFrom(q => q.Id))
                .ForMember(v => v.Tasks, c => c.MapFrom(q => q.Tasks))
                .ForMember(v => v.Name, c => c.MapFrom(q => q.Name))
                .ForMember(v => v.Description, c => c.MapFrom(q => q.Description))
                .ForMember(v => v.IsAvailable, c => c.MapFrom(q => q.IsAvailable));
            CreateMap<TaskModel, TaskViewModel>()
                .ForMember(v => v.Id, c => c.MapFrom(t => t.Id))
                .ForMember(v => v.IsMultipy, c => c.MapFrom(t => t.IsMultiply))
                .ForMember(v => v.Answers, c => c.MapFrom(t => t.Answers));
            CreateMap<AnswerModel, AnswerViewModel>()
                .ForMember(v => v.Id, c => c.MapFrom(a => a.Id))
                .ForMember(v => v.IsCorrect, c => c.MapFrom(a => a.IsCorrect))
                .ForMember(v => v.Text, c => c.MapFrom(a => a.Text));
        }

        /// <summary>
        ///     Create map to core models.
        /// </summary>
        private void MapingQuiz()
        {
            CreateMap<QuizViewModel, QuizModel>()
               .ForMember(q => q.Name, c => c.MapFrom(v => v.Name))
               .ForMember(q => q.Description, c => c.MapFrom(v => v.Description))
               .ForMember(q => q.IsAvailable, c => c.MapFrom(v => v.IsAvailable));
            CreateMap<TaskViewModel, TaskModel>()
                .ForMember(t => t.Queistion, c => c.MapFrom(v => v.Question))
                .ForMember(t => t.QuizId, c => c.MapFrom(v => v.QuizId));
            CreateMap<AnswerViewModel, AnswerModel>()
                .ForMember(a => a.IsCorrect, c => c.MapFrom(v => v.IsCorrect))
                .ForMember(a => a.Text, c => c.MapFrom(v => v.Text));
        }
    }
}
