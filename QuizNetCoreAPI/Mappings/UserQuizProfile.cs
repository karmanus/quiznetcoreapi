﻿using ApplicationCore.Models;
using AutoMapper;
using QuizNetCoreAPI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuizNetCoreAPI.Mappings
{
    public class UserQuizProfile : Profile
    {
        public UserQuizProfile()
        {
            MapingUserQuizView();
            MapingUserQuiz();
        }

        private void MapingUserQuizView()
        {
            CreateMap<UserQuizModel, UserQuizViewModel>()
                .ForMember(v => v.Id, c => c.MapFrom(qm => qm.Id))
                .ForMember(v => v.IsFinished, c => c.MapFrom(qm => qm.IsFinished))
                .ForMember(v => v.UserName, c => c.MapFrom(qm => qm.UserName));
            CreateMap<UserTaskModel, UserTaskViewModel>()
                .ForMember(v => v.Id, c => c.MapFrom(tm => tm.Id))
                .ForMember(v => v.Question, c => c.MapFrom(tm => tm.Question));
            CreateMap<UserAnswerModel, UserAnswerViewModel>()
                .ForMember(v => v.Id, c => c.MapFrom(am => am.Id))
                .ForMember(v=>v.Text, c=>c.MapFrom(am=>am.Text))
                .ForMember(v=>v.IsSelected,c=>c.MapFrom(am=> am.IsSelected));
        }

        private void MapingUserQuiz()
        {
            CreateMap<UserQuizViewModel, UserQuizModel >()
                .ForMember(v => v.Id, c => c.MapFrom(qm => qm.Id))
                .ForMember(v => v.IsFinished, c => c.MapFrom(qm => qm.IsFinished))
                .ForMember(v => v.UserName, c => c.MapFrom(qm => qm.UserName));
            CreateMap<UserTaskViewModel , UserTaskModel>()
                .ForMember(v => v.Id, c => c.MapFrom(tm => tm.Id))
                .ForMember(v => v.Question, c => c.MapFrom(tm => tm.Question));
            CreateMap<UserAnswerViewModel, UserAnswerModel>()
                .ForMember(v => v.Id, c => c.MapFrom(am => am.Id))
                .ForMember(v => v.Text, c => c.MapFrom(am => am.Text))
                .ForMember(v => v.IsSelected, c => c.MapFrom(am => am.IsSelected));
        }
    }
}
