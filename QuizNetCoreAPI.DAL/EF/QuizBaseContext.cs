﻿using Microsoft.EntityFrameworkCore;
using QuizNetCoreAPI.DAL.Entities;

namespace QuizNetCoreAPI.DAL.EF
{
    public partial class QuizBaseContext : DbContext
    {
        public QuizBaseContext()
        {
        }

        public QuizBaseContext(DbContextOptions<QuizBaseContext> options)
            : base(options)
        {
        }

        public virtual DbSet<QuizTask> QuizTasks { get; set; }
        public virtual DbSet<Quize> Quizes { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<TaskAnswer> TaskAnswers { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserAnswer> UserAnswers { get; set; }
        public virtual DbSet<UserQuize> UserQuizes { get; set; }
        public virtual DbSet<UserTask> UserTasks { get; set; }
        public virtual DbSet<UsersRole> UsersRoles { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=.\\SQLEXPRESS;Database=QuizBase;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Cyrillic_General_CI_AS");

            modelBuilder.Entity<QuizTask>(entity =>
            {
                entity.Property(e => e.Question)
                    .IsRequired()
                    .HasMaxLength(300);

                entity.HasOne(d => d.Quiz)
                    .WithMany(p => p.QuizTasks)
                    .HasForeignKey(d => d.QuizId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_QuizesTasks_Quizes");
            });

            modelBuilder.Entity<Quize>(entity =>
            {
                entity.Property(e => e.Discription).HasMaxLength(200);

                entity.Property(e => e.IsAvailable)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.HasKey(e => e.Role1);

                entity.Property(e => e.Role1)
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasColumnName("Role");
            });

            modelBuilder.Entity<TaskAnswer>(entity =>
            {
                entity.Property(e => e.AnswerText)
                    .IsRequired()
                    .HasMaxLength(300);

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.TaskAnswers)
                    .HasForeignKey(d => d.TaskId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Answers_QuizesTasks");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasKey(e => e.NickName);

                entity.Property(e => e.NickName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UserAnswer>(entity =>
            {
                entity.HasKey(e => new { e.TaskId, e.AnswerId })
                    .HasName("PK_StartedTasksQuestions");

                entity.HasOne(d => d.Answer)
                    .WithMany(p => p.UserAnswers)
                    .HasForeignKey(d => d.AnswerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_StartedTasksQuestions_Answers");

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.UserAnswers)
                    .HasForeignKey(d => d.TaskId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_StartedTaskQuestions_TasksStorageOfStartedTests");
            });

            modelBuilder.Entity<UserQuize>(entity =>
            {
                entity.Property(e => e.QuizUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Quiz)
                    .WithMany(p => p.UserQuizes)
                    .HasForeignKey(d => d.QuizId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_StartedQuizes_Quizes");

                entity.HasOne(d => d.QuizUserNavigation)
                    .WithMany(p => p.UserQuizes)
                    .HasForeignKey(d => d.QuizUser)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_StartedQuizes_Users");
            });

            modelBuilder.Entity<UserTask>(entity =>
            {
                entity.HasOne(d => d.Task)
                    .WithMany(p => p.UserTasks)
                    .HasForeignKey(d => d.TaskId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TasksStorageOfStartedTests_QuizTasks");

                entity.HasOne(d => d.UserQuiz)
                    .WithMany(p => p.UserTasks)
                    .HasForeignKey(d => d.UserQuizId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TasksStorageOfStartedTests_StartedQuizes");
            });

            modelBuilder.Entity<UsersRole>(entity =>
            {
                entity.HasKey(e => new { e.UserNickName, e.Role });

                entity.Property(e => e.UserNickName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Role)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.HasOne(d => d.RoleNavigation)
                    .WithMany(p => p.UsersRoles)
                    .HasForeignKey(d => d.Role)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UsersRoles_Roles");

                entity.HasOne(d => d.UserNickNameNavigation)
                    .WithMany(p => p.UsersRoles)
                    .HasForeignKey(d => d.UserNickName)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UsersRoles_Users");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
