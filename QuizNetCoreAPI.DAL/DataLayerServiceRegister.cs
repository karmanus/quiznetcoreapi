﻿using Microsoft.Extensions.DependencyInjection;
using QuizNetCoreAPI.DAL.Interfaces;
using QuizNetCoreAPI.DAL.Repositories;
using QuizNetCoreAPI.DAL.Repositories.Verifiers;

namespace QuizNetCoreAPI.DAL
{
    public static class DataLayerServiceRegister
    {
        public static void Configure(IServiceCollection services)
        {
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IQuizEditorRepository, QuizEditorRepository>();
            services.AddScoped<IQuizRepository, QuizRepository>();
            services.AddScoped<IUserQuizRepository, UserQuizRepository>();   
            services.AddScoped<IUserQuizVerifier, UserQuizVerifier>();
        }
    }
}
