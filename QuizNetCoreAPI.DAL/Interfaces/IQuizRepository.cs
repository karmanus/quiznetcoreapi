﻿using QuizNetCoreAPI.DAL.Entities;
using System.Collections.Generic;


namespace QuizNetCoreAPI.DAL.Interfaces
{
    /// <summary>
    ///     Quiz repository.
    /// </summary>
    public interface IQuizRepository
    {
        /// <summary>
        ///     Get quiz.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Quize</returns>
        public Quize GetQuiz(int id);

        /// <summary>
        ///     Get All Quizes. 
        /// </summary>
        /// <param name="onlyAvailable">Show only availeble quizes</param>
        /// <returns>All Quizes</returns>
        public IEnumerable<Quize> GetQuizes();

        public IEnumerable<Quize> GetAvailebleQuizes();
    }
}
