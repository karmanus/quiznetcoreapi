﻿using QuizNetCoreAPI.DAL.Entities;

namespace QuizNetCoreAPI.DAL.Interfaces
{
    /// <summary>
    ///     User repository.
    /// </summary>
    public interface IUserRepository
    {
        /// <summary>
        ///     Get user.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns>User</returns>
        public User GetUser(string userName, string password);
    }
}
