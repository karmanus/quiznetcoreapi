﻿using QuizNetCoreAPI.DAL.Entities;
using System.Collections.Generic;

namespace QuizNetCoreAPI.DAL.Interfaces
{
    /// <summary>
    ///     User quiz repository.
    /// </summary>
    public interface IUserQuizRepository
    {
        /// <summary>
        ///     Get user quiz by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>User quiz</returns>
        public UserQuize GetUserQuiz(int id);

        /// <summary>
        ///     Get all user quizes
        /// </summary>
        /// <returns>User quizes</returns>
        public IEnumerable<UserQuize> GetAllUsersQuizes();

        /// <summary>
        ///     Get user quizes by user name.
        /// </summary>
        /// <param name="UserName"></param>
        /// <returns>User quizes</returns>
        public IEnumerable<UserQuize> GetUserQuizes(string UserName);

        /// <summary>
        /// Select answer.
        /// </summary>
        /// <param name="AnswerId"></param>
        /// <param name="userTaskId"></param>
        /// <param name="choice"></param>
        /// <returns>User answer ID</returns>
        public int SelectAnswer(int AnswerId, int userTaskId, bool choice);

        /// <summary>
        /// Create new user quize.
        /// </summary>
        /// <param name="quizId"></param>
        /// <param name="userName"></param>
        /// <returns>User Quiz</returns>
        public UserQuize AddNewUserQuiz(int quizId, string userName);

        /// <summary>
        /// Finished user quiz if all answers are correct.
        /// </summary>
        /// <param name="userQuizId"></param>
        /// <returns></returns>
        public int FinishUserQuiz(int userQuizId);
    }
}
