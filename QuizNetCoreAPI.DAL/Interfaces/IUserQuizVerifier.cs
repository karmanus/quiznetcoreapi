﻿using QuizNetCoreAPI.DAL.Entities;

namespace QuizNetCoreAPI.DAL.Interfaces
{
    public interface IUserQuizVerifier
    {
        public bool VerifyUserAnswers(UserTask userTask, QuizTask quizTask);
    }
}
