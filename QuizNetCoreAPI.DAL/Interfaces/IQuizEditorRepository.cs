﻿using QuizNetCoreAPI.DAL.Entities;

namespace QuizNetCoreAPI.DAL.Interfaces
{
    /// <summary>
    /// Interfaces for editing Quiz
    /// </summary>
    public interface IQuizEditorRepository
    {
        /// <summary>
        ///     Add quiz.
        /// </summary>
        /// <param name="quize"></param>
        /// <returns>Quiz ID</returns>
        public int AddQuiz(Quize quize);

        /// <summary>
        ///     Delete quiz.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public int DeleteQuiz(int id);

        /// <summary>
        ///     Change answer text.        
        /// </summary>
        /// <param name="answerId"></param>
        /// <param name="answerText"></param>
        /// <returns>Answer ID</returns>
        public int ChangeAnswerText(int answerId, string answerText);

        /// <summary>
        ///     Change question text.
        /// </summary>
        /// <param name="questionId"></param>
        /// <param name="questionText"></param>
        /// <returns>Quiz Task ID</returns>
        public int ChangeQuestionText(int questionId, string questionText);

        /// <summary>
        ///     Change answer filed status IsCorrect.
        /// </summary>
        /// <param name="answerId"></param>
        /// <param name="IsCorrect"></param>
        /// <returns>Answer ID</returns>
        public int EditAnswerStatus(int answerId, bool IsCorrect);

        /// <summary>
        ///     Add answer to task.
        /// </summary>
        /// <param name="answer"></param>
        /// <returns>Answer ID</returns>
        public int AddAnswer(TaskAnswer answer);

        /// <summary>
        ///     Delete answer
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Answer ID</returns>
        public int DeleteAnswer(int id);

        /// <summary>
        ///     Add quiz task.
        /// </summary>
        /// <param name="task"></param>
        /// <returns>Task ID</returns>
        public int AddQuizTask(QuizTask task);

        /// <summary>
        ///     Delete task
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Task ID</returns>
        public int DeleteTaks(int id);
    }
}
