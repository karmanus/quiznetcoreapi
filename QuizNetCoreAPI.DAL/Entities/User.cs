﻿using System.Collections.Generic;

namespace QuizNetCoreAPI.DAL.Entities
{
    public partial class User
    {
        public User()
        {
            UserQuizes = new HashSet<UserQuize>();
            UsersRoles = new HashSet<UsersRole>();
        }

        public string NickName { get; set; }
        public string Password { get; set; }

        public virtual ICollection<UserQuize> UserQuizes { get; set; }
        public virtual ICollection<UsersRole> UsersRoles { get; set; }
    }
}
