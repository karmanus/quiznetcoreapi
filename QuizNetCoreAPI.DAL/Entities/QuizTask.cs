﻿using System.Collections.Generic;

namespace QuizNetCoreAPI.DAL.Entities
{
    public partial class QuizTask
    {
        public QuizTask()
        {
            TaskAnswers = new HashSet<TaskAnswer>();
            UserTasks = new HashSet<UserTask>();
        }

        public int Id { get; set; }
        public int QuizId { get; set; }
        public string Question { get; set; }

        public virtual Quize Quiz { get; set; }
        public virtual ICollection<TaskAnswer> TaskAnswers { get; set; }
        public virtual ICollection<UserTask> UserTasks { get; set; }
    }
}
