﻿namespace QuizNetCoreAPI.DAL.Entities
{
    public partial class UserAnswer
    {
        public int TaskId { get; set; }
        public int AnswerId { get; set; }
        public bool IsSelected { get; set; }

        public virtual TaskAnswer Answer { get; set; }
        public virtual UserTask Task { get; set; }
    }
}
