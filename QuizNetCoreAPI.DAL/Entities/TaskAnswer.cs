﻿using System.Collections.Generic;

namespace QuizNetCoreAPI.DAL.Entities
{
    public partial class TaskAnswer
    {
        public TaskAnswer()
        {
            UserAnswers = new HashSet<UserAnswer>();
        }

        public int Id { get; set; }
        public int TaskId { get; set; }
        public bool IsCorrect { get; set; }
        public string AnswerText { get; set; }

        public virtual QuizTask Task { get; set; }
        public virtual ICollection<UserAnswer> UserAnswers { get; set; }
    }
}
