﻿namespace QuizNetCoreAPI.DAL.Entities
{
    public partial class UsersRole
    {
        public string UserNickName { get; set; }
        public string Role { get; set; }

        public virtual Role RoleNavigation { get; set; }
        public virtual User UserNickNameNavigation { get; set; }
    }
}
