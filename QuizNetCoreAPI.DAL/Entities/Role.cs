﻿using System.Collections.Generic;

namespace QuizNetCoreAPI.DAL.Entities
{
    public partial class Role
    {
        public Role()
        {
            UsersRoles = new HashSet<UsersRole>();
        }

        public string Role1 { get; set; }

        public virtual ICollection<UsersRole> UsersRoles { get; set; }
    }
}
