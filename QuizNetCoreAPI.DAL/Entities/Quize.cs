﻿using System.Collections.Generic;

namespace QuizNetCoreAPI.DAL.Entities
{
    public partial class Quize
    {
        public Quize()
        {
            QuizTasks = new HashSet<QuizTask>();
            UserQuizes = new HashSet<UserQuize>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Discription { get; set; }
        public bool? IsAvailable { get; set; }
        public virtual ICollection<QuizTask> QuizTasks { get; set; }
        public virtual ICollection<UserQuize> UserQuizes { get; set; }
    }
}
