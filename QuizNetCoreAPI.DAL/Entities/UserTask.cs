﻿using System.Collections.Generic;

namespace QuizNetCoreAPI.DAL.Entities
{
    public partial class UserTask
    {
        public UserTask()
        {
            UserAnswers = new HashSet<UserAnswer>();
        }

        public int Id { get; set; }
        public int UserQuizId { get; set; }
        public int TaskId { get; set; }

        public virtual QuizTask Task { get; set; }
        public virtual UserQuize UserQuiz { get; set; }
        public virtual ICollection<UserAnswer> UserAnswers { get; set; }
    }
}
