﻿using System.Collections.Generic;

namespace QuizNetCoreAPI.DAL.Entities
{
    public partial class UserQuize
    {
        public UserQuize()
        {
            UserTasks = new HashSet<UserTask>();
        }

        public int Id { get; set; }
        public int QuizId { get; set; }
        public string QuizUser { get; set; }
        public bool IsFinished { get; set; }

        public virtual Quize Quiz { get; set; }
        public virtual User QuizUserNavigation { get; set; }
        public virtual ICollection<UserTask> UserTasks { get; set; }
    }
}
