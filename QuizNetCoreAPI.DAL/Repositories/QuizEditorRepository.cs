﻿using QuizNetCoreAPI.DAL.EF;
using QuizNetCoreAPI.DAL.Entities;
using QuizNetCoreAPI.DAL.Interfaces;
using System;
using System.Linq;

namespace QuizNetCoreAPI.DAL.Repositories
{
    /// <inheritdoc/>    
    public class QuizEditorRepository : IQuizEditorRepository, IDisposable
    {
        private readonly QuizBaseContext _context;
        private const int maxAnswersCount = 5;
        private const int maxTasksCount = 10;
        private const int minCorecctAnswersCount = 1;
        private const int minAnswersCount = 2;

        public QuizEditorRepository()
        {
            _context = new QuizBaseContext();
        }

        public int AddQuiz(Quize quize)
        {
            _context.Add(quize);
            return _context.SaveChanges();
        }

        public int AddQuizTask(QuizTask task)
        {
            var isRunnningTests = CheckForRunningTests(task.QuizId);
            var taskCount = GetQuizTaskCount(task.QuizId);

            if (isRunnningTests || taskCount >= maxTasksCount)
            {
                return default;
            }

            _context.Add(task);
            return _context.SaveChanges();
        }

        public int AddAnswer(TaskAnswer answer)
        {
            var quizId = _context.QuizTasks.Single(t => t.Id == answer.TaskId).QuizId;
            var isRunnningTests = CheckForRunningTests(quizId);
            var answersCount = GetAnnwersCount(answer.TaskId);

            if (isRunnningTests || answersCount >= maxAnswersCount)
            {
                return default;
            }

            _context.TaskAnswers.Add(answer);
            return _context.SaveChanges();
        }

        public int EditAnswerStatus(int answerId, bool isCorrect)
        {
            var answer = _context.TaskAnswers.Single(a => a.Id == answerId);
            var quizId = _context.Quizes.Single(q => q.Id == _context.QuizTasks.Single(t => t.Id == answer.TaskId).QuizId).Id;
            var isRunnningTests = CheckForRunningTests(quizId);
            var correctAnswersCount = _context.TaskAnswers.Where(a => a.TaskId == answer.TaskId && a.IsCorrect).Count();

            if (isRunnningTests || (!answer.IsCorrect && correctAnswersCount <= minCorecctAnswersCount))
            {
                return default;
            }

            answer.IsCorrect = isCorrect;
            _context.SaveChanges();
            return answerId;
        }

        public int ChangeAnswerText(int answerId, string answerText)
        {
            var answer = _context.TaskAnswers.Single(a => a.Id == answerId);
            var quizId = _context.Quizes.Single(q => q.Id == _context.QuizTasks.Single(t => t.Id == answer.TaskId).QuizId).Id;

            if (CheckForRunningTests(quizId))
            {
                return default;
            }

            answer.AnswerText = answerText;
            _context.SaveChanges();
            return answerId;
        }

        public int ChangeQuestionText(int questionId, string questionText)
        {
            var task = _context.QuizTasks.Single(t => t.Id == questionId);
            var quizId = _context.Quizes.Single(q => q.Id == task.QuizId).Id;
            var isRunnningTests = CheckForRunningTests(quizId);

            if (isRunnningTests)
            {
                return default;
            }

            task.Question = questionText;
            _context.SaveChanges();
            return questionId;
        }

        public int DeleteQuiz(int id)
        {
            var isRunnningTests = CheckForRunningTests(id);

            if (isRunnningTests)
            {
                return default;
            }

            _context.Remove(id);
            _context.SaveChanges();
            return id;
        }

        public int DeleteTaks(int id)
        {
            var task = _context.QuizTasks.SingleOrDefault(task => task.Id == id);
            var isRunnningTests = CheckForRunningTests(id);
            var quizTasksCount = GetQuizTaskCount(task.QuizId);

            if (task == null || isRunnningTests || quizTasksCount >= maxTasksCount)
            {
                return default;
            }

            _context.Remove(task);
            return _context.SaveChanges();
        }

        public int DeleteAnswer(int id)
        {
            var answer = _context.TaskAnswers.Single(a => a.Id == id);
            var quizId = _context.Quizes.Single(q => q.Id == _context.QuizTasks.Single(t => t.Id == answer.TaskId).QuizId).Id;
            var isRunnningTests = CheckForRunningTests(quizId);
            var answersCount = GetAnnwersCount(answer.TaskId);

            if (isRunnningTests || answersCount <= minAnswersCount)
            {
                return default;
            }

            _context.Remove(answer);
            _context.SaveChanges();
            return id;
        }

        private int GetAnnwersCount(int taskId)
        {
            return _context.TaskAnswers.Where(a => a.TaskId == taskId).Count();
        }

        private int GetQuizTaskCount(int quizId)
        {
            return _context.QuizTasks.Where(t => t.QuizId == quizId).Count();
        }

        private bool CheckForRunningTests(int quizId)
        {
            return _context.UserQuizes.Any(q => q.QuizId == quizId && !q.IsFinished);
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
