﻿namespace QuizNetCoreAPI.DAL.Repositories.Verifiers
{
    public class ApproximateAnswerModel
    {
        public int Id { get; set; }
        public bool IsSelected { get; set; }
    }
}
