﻿using QuizNetCoreAPI.DAL.Entities;
using QuizNetCoreAPI.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizNetCoreAPI.DAL.Repositories.Verifiers
{
    public class UserQuizVerifier : IUserQuizVerifier
    {
        public bool VerifyUserAnswers(UserTask userTask, QuizTask quizTask)
        {
            if (userTask.TaskId != quizTask.Id)
            {
                return false;
            }

            var taskAnswers = quizTask.TaskAnswers.Select(a => new ApproximateAnswerModel() { Id = a.Id, IsSelected = a.IsCorrect }).OrderBy(a => a.Id);
            var userAnswers = userTask.UserAnswers.Select(ua => new ApproximateAnswerModel() { Id = ua.TaskId, IsSelected = ua.IsSelected }).OrderBy(ua => ua.Id);

            return userAnswers.SequenceEqual(taskAnswers);
        }
    }
}
