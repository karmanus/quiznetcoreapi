﻿using Microsoft.EntityFrameworkCore;
using QuizNetCoreAPI.DAL.EF;
using QuizNetCoreAPI.DAL.Entities;
using QuizNetCoreAPI.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace QuizNetCoreAPI.DAL.Repositories
{
    /// <inheritdoc/>
    public class QuizRepository : IQuizRepository, IDisposable
    {
        private readonly QuizBaseContext _context;

        public QuizRepository()
        {
            _context = new QuizBaseContext();
        }
        public Quize GetQuiz(int id)
        {
            return _context.Quizes.Include(q => q.QuizTasks)
                .ThenInclude(t => t.TaskAnswers)
                .Single(q => q.Id == id);
        }

        public IEnumerable<Quize> GetQuizes()
        {
            return _context.Quizes.ToList();
        }

        public IEnumerable<Quize> GetAvailebleQuizes()
        {
            return _context.Quizes.Where(q => (bool)q.IsAvailable).ToList();
        }

        public void Dispose()
        {
            _context.Dispose();
        }        
    }
}
