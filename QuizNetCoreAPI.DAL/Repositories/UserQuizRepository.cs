﻿using Microsoft.EntityFrameworkCore;
using QuizNetCoreAPI.DAL.EF;
using QuizNetCoreAPI.DAL.Entities;
using QuizNetCoreAPI.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace QuizNetCoreAPI.DAL.Repositories
{
    /// <inheritdoc/>
    public class UserQuizRepository : IUserQuizRepository, IDisposable
    {
        private readonly QuizBaseContext _context;
        private readonly IUserQuizVerifier _verifier;

        public UserQuizRepository(IUserQuizVerifier verifier)
        {
            _verifier = verifier;
            _context = new QuizBaseContext();
        }

        public UserQuize AddNewUserQuiz(int quizId, string userName)
        {
            var userquiz = _context.UserQuizes.SingleOrDefault(uq => uq.QuizId == quizId && uq.QuizUser == userName && !uq.IsFinished);

            if (userquiz != null)
            {
                return userquiz;
            }

            var newUserQuiz = CreateUserQuize(quizId,userName);
            _context.UserQuizes.Add(newUserQuiz);
            var id = _context.SaveChanges();
            return _context.UserQuizes.Single(uq=>uq.Id == id);
        }

        public int FinishUserQuiz(int userQuizId)
        {
            var userQuiz = _context.UserQuizes.Single(uq => uq.Id == userQuizId);
            userQuiz.IsFinished = true;
            return _context.SaveChanges();
        }

        public IEnumerable<UserQuize> GetAllUsersQuizes()
        {
            return _context.UserQuizes.ToList();
        }

        public UserQuize GetUserQuiz(int id)
        {
            return _context.UserQuizes.Single(uq => uq.Id == id);
        }

        public IEnumerable<UserQuize> GetUserQuizes(string UserName)
        {
            return _context.UserQuizes.Where(uq => uq.QuizUser == UserName).ToList();
        }

        public int SelectAnswer(int answerId, int userTaksId, bool choice)
        {
            var answer = _context.UserAnswers.SingleOrDefault(a => a.AnswerId == answerId && a.TaskId == userTaksId);

            if (answer == null)
            {
                return default;
            }

            answer.IsSelected = choice;
            return _context.SaveChanges();
        }

        private UserQuize CreateUserQuize(int quizId, string userName)
        {
            var quiz = _context.Quizes.Include(q => q.QuizTasks)
                .ThenInclude(t => t.TaskAnswers)
                .Single(q=>q.Id == quizId);

            return new UserQuize()
            {
                IsFinished = false,
                QuizUser = userName,
                QuizId = quiz.Id,
                UserTasks = quiz.QuizTasks.Select(t => ConverQuizTaskToUserTask(t)).ToList()
            };
        }

        private UserTask ConverQuizTaskToUserTask(QuizTask quizTask)
        {
            return new UserTask()
            {
                TaskId = quizTask.Id,
                UserAnswers = quizTask.TaskAnswers.Select(a => ConvertTaskAnswerToUserAnswer(a)).ToList()
            };
        }

        private UserAnswer ConvertTaskAnswerToUserAnswer(TaskAnswer answer)
        {
            return new UserAnswer()
            {
                AnswerId = answer.Id,
                IsSelected = false
            };
        }   

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
