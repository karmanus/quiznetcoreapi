﻿using Microsoft.EntityFrameworkCore;
using QuizNetCoreAPI.DAL.EF;
using QuizNetCoreAPI.DAL.Entities;
using QuizNetCoreAPI.DAL.Interfaces;
using System;
using System.Linq;

namespace QuizNetCoreAPI.DAL.Repositories
{
    /// <inheritdoc/>
    public class UserRepository : IUserRepository, IDisposable
    {
        private readonly QuizBaseContext _context;

        public UserRepository()
        {
            _context = new QuizBaseContext();
        }

        public User GetUser(string userName, string password)
        {
            return _context.Users.Include(u => u.UsersRoles).Single(u => u.NickName == userName && u.Password == password);
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
