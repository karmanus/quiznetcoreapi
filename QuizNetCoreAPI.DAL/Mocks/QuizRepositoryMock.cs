﻿using System.Collections.Generic;
using System.Linq;
using QuizNetCoreAPI.DAL.Entities;
using QuizNetCoreAPI.DAL.Interfaces;

namespace QuizNetCoreAPI.DAL.Mocks
{
    /// <summary>
    ///     Mock for imitation quiz repository.
    /// <inheritdoc/>  
    /// </summary>
    public class QuizRepositoryMock : IQuizRepository
    {
        private readonly List<Quize> _quizes;

        public QuizRepositoryMock()
        {
            _quizes = new List<Quize>() {

                new Quize()
                {
                    Id = 1,
                    Discription= "TestQuiz",
                    IsAvailable = true, Name= "Test",
                    QuizTasks = new List<QuizTask>()
                    {
                        new QuizTask()
                        {
                            Id = 1,
                            Question="TestQuestion",
                            QuizId = 1,
                            TaskAnswers = new List<TaskAnswer>()
                            {
                                new TaskAnswer()
                                {
                                    Id = 1,
                                    AnswerText = "TestText",
                                    TaskId = 1, IsCorrect = true
                                },
                                new TaskAnswer()
                                {
                                    Id = 2,
                                    AnswerText = "TestText2",
                                    TaskId = 1,
                                    IsCorrect = false
                                }
                            }
                        },

                        new QuizTask()
                        {
                            Id = 2,
                            Question="TestQuestion2",
                            QuizId = 1,
                            TaskAnswers = new List<TaskAnswer>()
                            {
                                new TaskAnswer()
                                {
                                    Id = 3,
                                    AnswerText = "TestText3",
                                    TaskId = 2,
                                    IsCorrect = true
                                },
                                new TaskAnswer()
                                {
                                    Id = 4,
                                    AnswerText = "TestText4",
                                    TaskId = 2,
                                    IsCorrect = false
                                }
                            }
                        }
                    }
                },
                 new Quize()
                {
                    Id = 1,
                    Discription= "TestQuiz2",
                    IsAvailable = true, Name= "Test2",
                    QuizTasks = new List<QuizTask>()
                    {
                        new QuizTask()
                        {
                            Id = 1,
                            Question="TestQuestion3",
                            QuizId = 1,
                            TaskAnswers = new List<TaskAnswer>()
                            {
                                new TaskAnswer()
                                {
                                    Id = 1,
                                    AnswerText = "TestText5",
                                    TaskId = 1, IsCorrect = true
                                },
                                new TaskAnswer()
                                {
                                    Id = 2,
                                    AnswerText = "TestText6",
                                    TaskId = 1,
                                    IsCorrect = false
                                }
                            }
                        },

                        new QuizTask()
                        {
                            Id = 2,
                            Question="TestQuestion4",
                            QuizId = 1,
                            TaskAnswers = new List<TaskAnswer>()
                            {
                                new TaskAnswer()
                                {
                                    Id = 3,
                                    AnswerText = "TestText7",
                                    TaskId = 2,
                                    IsCorrect = true
                                },
                                new TaskAnswer()
                                {
                                    Id = 4,
                                    AnswerText = "TestText8",
                                    TaskId = 2,
                                    IsCorrect = false
                                }
                            }
                        }
                    }
                }
            };
        }

        public Quize GetQuiz(int id)
        {
            return _quizes[id];
        }

        public IEnumerable<Quize> GetQuizes()
        {
            return _quizes;
        }

        public IEnumerable<Quize> GetAvailebleQuizes()
        {
            return _quizes.Where(q => (bool)q.IsAvailable).ToList();
        }
    }
}
