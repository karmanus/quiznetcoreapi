﻿using ApplicationCore.Models;
using QuizNetCoreAPI.DAL.Entities;

namespace ApplicationCore.Interfaces
{
    /// <summary>
    ///     Convertor quiz objects to quiz models and quiz model to quiz objects.
    /// </summary>
    public interface IQuizConvertor
    {
        /// <summary>
        ///     Convert quiz model to quiz.
        /// </summary>
        /// <param name="quizModel"></param>
        /// <returns>Quiz</returns>
        public Quize ConvertToQuiz(QuizModel quizModel);

        /// <summary>
        ///     Convert task model to quiz task.
        /// </summary>
        /// <param name="taskModel"></param>
        /// <returns>Quiz task</returns>
        public QuizTask ConvertToTask(TaskModel taskModel);

        /// <summary>
        ///     Convert answer model to task answer.
        /// </summary>
        /// <param name="answerModel"></param>
        /// <returns></returns>
        public TaskAnswer ConvertToAnswer(AnswerModel answerModel);

        /// <summary>
        ///     Conver quiz to quiz model.
        /// </summary>
        /// <param name="quiz"></param>
        /// <returns>Quiz model</returns>
        public QuizModel ConvertQuizToModel(Quize quiz);

        /// <summary>
        ///     Convert quiz task to task model.
        /// </summary>
        /// <param name="task"></param>
        /// <returns>Task model</returns>
        public TaskModel ConverToTaskModel(QuizTask task);

        /// <summary>
        ///     Convert task answer to answer model.
        /// </summary>
        /// <param name="answer"></param>
        /// <returns>Answer model</returns>
        public AnswerModel ConverToAnswerModel(TaskAnswer answer);
    }
}
