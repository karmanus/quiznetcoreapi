﻿using System.Collections.Generic;
using ApplicationCore.Models;

/// <summary>
///     Quiz Mapper.
/// </summary>
namespace ApplicationCore.Interfaces
{
    public interface IQuizMapper
    {
        /// <summary>
        ///     Get quiz.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Quiz model</returns>
        public QuizModel GetQuiz(int id);

        /// <summary>
        ///     Get quizes. 
        /// </summary>
        /// <returns>Quiz models</returns>
        public IEnumerable<QuizModel> GetQuizes();

        /// <summary>
        ///     Get availeble quizes.
        /// </summary>
        /// <returns>Quiz models</returns>
        public IEnumerable<QuizModel> GetAvailebleQuizes();
    }
}
