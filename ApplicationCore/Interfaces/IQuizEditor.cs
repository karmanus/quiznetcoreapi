﻿using ApplicationCore.Models;

namespace ApplicationCore.Interfaces
{
    /// <summary>
    ///     Quize editing.
    /// </summary>
    public interface IQuizEditor
    {
        /// <summary>
        ///     Create quiz.
        /// </summary>
        /// <param name="quize"></param>
        /// <returns>Quize ID</returns>
        public int CreateQuiz(QuizModel quize);

        /// <summary>
        ///     Delete quiz.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Quiz ID</returns>
        public int DeleteQuiz(int id);

        /// <summary>
        ///     Change answer text.
        /// </summary>
        /// <param name="answerId"></param>
        /// <param name="answerText"></param>
        /// <returns>Answer ID</returns>
        public int ChangeAnswerText(int answerId, string answerText);

        /// <summary>
        ///     Change task text.
        /// </summary>
        /// <param name="questionId"></param>
        /// <param name="questionText"></param>
        /// <returns>Task ID</returns>
        public int ChangeQuestionText(int questionId, string questionText);

        /// <summary>
        ///     Change answer filed status IsCorrect.
        /// </summary>
        /// <param name="answerId"></param>
        /// <param name="IsCorrect"></param>
        /// <returns>Answer ID</returns>
        public int ChangeAnswerFieldIsCorrect(int answerId, bool IsCorrect);

        /// <summary>
        ///     Add answer.
        /// </summary>
        /// <param name="answer"></param>
        /// <returns>Answer ID</returns>
        public int AddAnswer(AnswerModel answer);

        /// <summary>
        ///     Delete answer.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Answer ID</returns>
        public int DeleteAnswer(int id);

        /// <summary>
        ///     Add quiz task. 
        /// </summary>
        /// <param name="task"></param>
        /// <returns>Quiz task ID</returns>
        public int AddQuizTask(TaskModel task);

        /// <summary>
        ///     Delete quiz task
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Task ID</returns>
        public int DeleteTask(int id);
    }
}
