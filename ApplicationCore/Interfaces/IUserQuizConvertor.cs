﻿using ApplicationCore.Models;
using QuizNetCoreAPI.DAL.Entities;

namespace ApplicationCore.Interfaces
{
    /// <summary>
    ///     Convertor user quiz.
    /// </summary>
    public interface IUserQuizConvertor
    {
        /// <summary>
        ///     Covert user quiz model to user quiz.
        /// </summary>
        /// <param name="quizModel"></param>
        /// <returns>User quiz</returns>
        public UserQuize ConvertToUserQuiz(UserQuizModel quizModel);

        /// <summary>
        ///     Convert user task model to user task.
        /// </summary>
        /// <param name="taskModel"></param>
        /// <returns>User task</returns>
        public UserTask ConvertToUserTask(UserTaskModel taskModel);

        /// <summary>
        ///     Convert user answer model to user answer.
        /// </summary>
        /// <param name="answerModel"></param>
        /// <returns>User answer</returns>
        public UserAnswer ConvertToUserAnswer(UserAnswerModel answerModel);

        /// <summary>
        ///     Convert user quiz to user quiz model.
        /// </summary>
        /// <param name="quiz"></param>
        /// <returns>User quiz model</returns>
        public UserQuizModel ConvertToUserQuizModel(UserQuize quiz);

        /// <summary>
        ///     Convert user task user task model.
        /// </summary>
        /// <param name="task"></param>
        /// <param name="questionText"></param>
        /// <returns>User task model</returns>
        public UserTaskModel ConverToUserTaskModel(UserTask task, string questionText);

        /// <summary>
        ///     Convert user answer to user answer model.
        /// </summary>
        /// <param name="answer"></param>
        /// <param name="answerText"></param>
        /// <returns>User answer model</returns>
        public UserAnswerModel ConverToUserAnswerModel(UserAnswer answer, string answerText);
    }
}
