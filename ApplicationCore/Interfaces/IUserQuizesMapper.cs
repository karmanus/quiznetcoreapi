﻿using ApplicationCore.Models;
using System.Collections.Generic;

namespace ApplicationCore.Interfaces
{
    /// <summary>
    ///     User quiz mapper.
    /// </summary>
    public interface IUserQuizesMapper
    {
        /// <summary>
        ///     Get user quiz by ID.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Quiz model</returns>
        public QuizModel GetUserQuize(int id);

        /// <summary>
        ///     Get all users quizes.
        /// </summary>
        /// <returns>Quiz models</returns>
        public IEnumerable<QuizModel> GetAllUsersQuizes();

        /// <summary>
        ///     Get user quizes by user name.
        /// </summary>
        /// <param name="userName"></param>
        /// <returns>Quiz models</returns>
        public IEnumerable<QuizModel> GetUserQuizes(string userName);
    }
}
