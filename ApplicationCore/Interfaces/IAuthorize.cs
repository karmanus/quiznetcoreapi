﻿using QuizNetCoreAPI.DAL.Entities;

namespace ApplicationCore.Interfaces
{

    /// <summary>
    ///     For Authorize.
    /// </summary>
    public interface IAuthorize
    {

        /// <summary>
        ///     Authorize.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns>User</returns>
        public User Authorize(string userName, string password);
    }
}
