﻿using QuizNetCoreAPI.DAL.Entities;

namespace ApplicationCore.Interfaces
{

    /// <summary>
    /// Class for takinq quiz.
    /// </summary>
    public interface IQuizRunner
    {
        /// <summary>
        ///     Start quiz.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="quizId"></param>
        /// <returns>User quize</returns>
        public UserQuize StartQuiz(string userName, int quizId);

        /// <summary>
        /// Select anser status.
        /// </summary>
        /// <param name="userAnswerId"></param>
        /// <param name="userTaskId"></param>
        /// <param name="answerStatus"></param>
        /// <returns>Answer ID</returns>
        public int SelectAnswer(int userAnswerId, int userTaskId, bool answerStatus);

        /// <summary>
        /// Finish quiz
        /// </summary>
        /// <param name="userQuizId"></param>
        /// <returns>User quiz ID</returns>
        public int FinishQuiz(int userQuizId);
    }
}
