﻿using System.Linq;
using System.Collections.Generic;
using ApplicationCore.Interfaces;
using ApplicationCore.Models;
using QuizNetCoreAPI.DAL.Interfaces;

namespace ApplicationCore.Mappers
{
    /// <inheritdoc/>
    public class QuizMapper : IQuizMapper
    {
        private readonly IQuizRepository _repository;
        private readonly IQuizConvertor _convertor;

        public QuizMapper(IQuizRepository repository, IQuizConvertor convertor)
        {
            _repository = repository;
            _convertor = convertor;
        }

        public IEnumerable<QuizModel> GetAvailebleQuizes()
        {
            return _repository.GetAvailebleQuizes().Select(q => _convertor.ConvertQuizToModel(q));
        }

        public QuizModel GetQuiz(int id)
        {
            return _convertor.ConvertQuizToModel(_repository.GetQuiz(id));
        }

        public IEnumerable<QuizModel> GetQuizes()
        {
            return _repository.GetQuizes().Select(q => _convertor.ConvertQuizToModel(q));
        }
    }
}
