﻿using ApplicationCore.Interfaces;
using ApplicationCore.Models;
using QuizNetCoreAPI.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Mappers
{
    public class UserQuizeMapper : IUserQuizesMapper
    {
        public IEnumerable<QuizModel> GetAllUsersQuizes()
        {
            throw new NotImplementedException();
        }

        public QuizModel GetUserQuize(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<QuizModel> GetUserQuizes(string userName)
        {
            throw new NotImplementedException();
        }
    }
}
