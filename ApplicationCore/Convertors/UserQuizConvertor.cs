﻿using ApplicationCore.Interfaces;
using ApplicationCore.Models;
using QuizNetCoreAPI.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Convertors
{
    //TODO
    public class UserQuizConvertor : IUserQuizConvertor
    {
        public UserAnswerModel ConverToUserAnswerModel(UserAnswer answer, string answerText)
        {
            return new UserAnswerModel()
            {
                Id = answer.AnswerId,
                IsSelected = answer.IsSelected,
                Text = answerText
            };
        }

        public UserTaskModel ConverToUserTaskModel(UserTask task, string questionText)
        {
            return new UserTaskModel()
            {
                Id = task.Id,
                Question = questionText,
                
            };
        }

        public UserAnswer ConvertToUserAnswer(UserAnswerModel answerModel)
        {
            return new UserAnswer()
            {
                
            };
        }

        public UserQuize ConvertToUserQuiz(UserQuizModel quizModel)
        {
            return new UserQuize()
            {

            };
        }

        public UserQuizModel ConvertToUserQuizModel(UserQuize quiz)
        {
            return new UserQuizModel()
            {

            };
        }

        public UserTask ConvertToUserTask(UserTaskModel taskModel)
        {
            return new UserTask()
            {
              Id = taskModel.Id,
              
            };
        }
    }
}
