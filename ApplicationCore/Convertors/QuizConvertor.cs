﻿using System.Linq;
using ApplicationCore.Interfaces;
using ApplicationCore.Models;
using QuizNetCoreAPI.DAL.Entities;

namespace ApplicationCore.Convertors
{
    /// <inheritdoc/>
    public class QuizConvertor : IQuizConvertor
    {
        public AnswerModel ConverToAnswerModel(TaskAnswer answer)
        {
            return new AnswerModel()
            {
                Id = answer.Id,
                IsCorrect = answer.IsCorrect,
                Text = answer.AnswerText
            };
        }

        public TaskModel ConverToTaskModel(QuizTask task)
        {
            return new TaskModel()
            {
                Id = task.Id,
                IsMultiply = task.TaskAnswers
                .Where(a => a.IsCorrect)
                .Count() > 1,
                Answers = task.TaskAnswers.Select(a => ConverToAnswerModel(a))
            };
        }

        public QuizModel ConvertQuizToModel(Quize quiz)
        {
            return new QuizModel()
            {
                Id = quiz.Id,
                Description = quiz.Discription,
                IsAvailable = (bool)quiz.IsAvailable,
                Name = quiz.Name,
                Tasks = quiz.QuizTasks.Select(t => ConverToTaskModel(t))
            };
        }

        public TaskAnswer ConvertToAnswer(AnswerModel answerModel)
        {
            return new TaskAnswer()
            {
                Id = 0,
                TaskId = answerModel.TaskId,
                IsCorrect = answerModel.IsCorrect,
                AnswerText = answerModel.Text
            };
        }

        public Quize ConvertToQuiz(QuizModel quizModel)
        {
            return new Quize()
            {
                Id = 0,
                Discription = quizModel.Description,
                IsAvailable = quizModel.IsAvailable,
                Name = quizModel.Name,
                QuizTasks = quizModel.Tasks.Select(t => ConvertToTask(t)).ToList()
            };
        }

        public QuizTask ConvertToTask(TaskModel taskModel)
        {
            return new QuizTask()
            {
                Id = taskModel.Id,
                Question = taskModel.Queistion,
                QuizId = taskModel.QuizId,
                TaskAnswers = taskModel.Answers.Select(a => ConvertToAnswer(a)).ToList()
            };
        }
    }
}
