﻿namespace ApplicationCore.Models
{
    public class UserAnswerModel
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public bool IsSelected { get; set; }
    }
}
