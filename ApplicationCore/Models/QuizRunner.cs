﻿using System.Collections.Generic;
using System.Linq;
using ApplicationCore.Interfaces;
using QuizNetCoreAPI.DAL.Entities;
using QuizNetCoreAPI.DAL.Interfaces;

namespace ApplicationCore.Models
{
    /// <inheritdoc/>
    public class QuizRunner : IQuizRunner
    {
        private readonly IUserQuizRepository _userQuizRepository;
        private readonly IQuizRepository _quizRepository;

        public QuizRunner(IUserQuizRepository userQuizRepository, IQuizRepository quizRepository)
        {
            _userQuizRepository = userQuizRepository;
            _quizRepository = quizRepository;
        }

        public int FinishQuiz(int userQuizId)
        {
            var userQuiz = _userQuizRepository.GetUserQuiz(userQuizId);
            var quiz = _quizRepository.GetQuiz(userQuiz.QuizId);
            var rightAnswersIdLists = quiz.QuizTasks.Select(t => t.TaskAnswers.Where(a => a.IsCorrect).Select(a => a.Id).OrderBy(i => i)).ToList();
            var userAnswersIdLists = userQuiz.UserTasks.Select(t => t.UserAnswers.Where(a => a.IsSelected).Select(a => a.AnswerId).OrderBy(i => i)).ToList();

            List<int> rightAnswersId = new List<int>();
            foreach (var ids in rightAnswersIdLists)
            {
                rightAnswersId.AddRange(ids);
            }

            List<int> userAnswersId = new List<int>();
            foreach (var ids in userAnswersIdLists)
            {
                userAnswersId.AddRange(ids);
            }

            if (rightAnswersId.Count() != userAnswersId.Count())
            {
                return 0;
            }
            for (int i = 0; i < userAnswersId.Count(); i++)
            {
                if (userAnswersId[i] != rightAnswersId[i])
                {
                    return 0;
                }
            }


            return _userQuizRepository.FinishUserQuiz(userQuizId);
        }

        public int SelectAnswer(int userAnswerId, int userTaskId, bool choice)
        {
            return _userQuizRepository.SelectAnswer(userAnswerId, userTaskId, choice);
        }

        public UserQuize StartQuiz(string userName, int quizId)
        {
            return _userQuizRepository.AddNewUserQuiz(quizId, userName);
        }
    }
}
