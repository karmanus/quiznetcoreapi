﻿using System.Collections.Generic;

namespace ApplicationCore.Models
{
    public class QuizModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsAvailable { get; set; }
        public IEnumerable<TaskModel> Tasks { get; set; }
    }
}
