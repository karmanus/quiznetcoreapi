﻿using System.Linq;
using System.Collections.Generic;
using QuizNetCoreAPI.DAL.Entities;

namespace ApplicationCore.Models
{
    public class UserModel
    {
        public UserModel(string name, IEnumerable<string> roles)
        {
            Name = name;
            Roles = roles;
        }

        public string Name { get; }
        public IEnumerable<string> Roles { get; }

        public static UserModel ConvertToUserModel(User user)
        {
            return new UserModel(user.NickName, user.UsersRoles.Select(r => r.Role));
        }
    }
}
