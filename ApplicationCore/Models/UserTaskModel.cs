﻿using System.Collections.Generic;

namespace ApplicationCore.Models
{
    public class UserTaskModel
    {
        public int Id { get; set; }
        public string Question { get; set; }
        public IEnumerable<UserAnswerModel> Answers { get; set; }
    }
}
