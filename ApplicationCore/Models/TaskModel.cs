﻿using System.Collections.Generic;

namespace ApplicationCore.Models
{
    public class TaskModel
    {
        public int Id { get; set; }
        public bool IsMultiply { get; set; }
        public string Queistion { get; set; }
        public int QuizId { get; set; }
        public IEnumerable<AnswerModel> Answers { get; set; }
    }
}
