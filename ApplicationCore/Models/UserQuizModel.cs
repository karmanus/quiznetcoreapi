﻿using System.Collections.Generic;

namespace ApplicationCore.Models
{
    public class UserQuizModel
    {
        public int Id { get; set; }
        public int QuizId { get; set; }
        public string UserName { get; set; }
        public bool IsFinished { get; set; }
        public IEnumerable<UserTaskModel> Tasks { get; set; }
    }
}
