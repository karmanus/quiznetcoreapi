﻿using ApplicationCore.Interfaces;
using ApplicationCore.Models;
using QuizNetCoreAPI.DAL.Interfaces;

namespace ApplicationCore.Editors
{
    /// <inheritdoc/>
    public class QuizEditor : IQuizEditor
    {
        private IQuizEditorRepository _repository;
        private IQuizConvertor _convertor;

        public QuizEditor(IQuizEditorRepository repository, IQuizConvertor convertor)
        {
            _repository = repository;
            _convertor = convertor;
        }

        public int AddAnswer(AnswerModel answer)
        {
            if (answer == null)
            {
                return default;                
            }
            return _repository.AddAnswer(_convertor.ConvertToAnswer(answer));
        }

        public int AddQuizTask(TaskModel task)
        {
            if (task==null)
            {
                return default;                
            }
            return _repository.AddQuizTask(_convertor.ConvertToTask(task));
        }

        public int ChangeAnswerFieldIsCorrect(int answerId, bool IsCorrect)
        {
            return _repository.EditAnswerStatus(answerId, IsCorrect);
        }

        public int ChangeAnswerText(int answerId, string answerText)
        {
            return _repository.ChangeAnswerText(answerId, answerText);
        }

        public int ChangeQuestionText(int questionId, string questionText)
        {
            return _repository.ChangeQuestionText(questionId, questionText);
        }

        public int CreateQuiz(QuizModel quiz)
        {
            if (quiz == null)
            {
                return default;
            }
            return _repository.AddQuiz(_convertor.ConvertToQuiz(quiz));            
        }

        public int DeleteAnswer(int id)
        {
            return _repository.DeleteAnswer(id);
        }

        public int DeleteQuiz(int id)
        {
            return _repository.DeleteQuiz(id);
        }

        public int DeleteTask(int id)
        {
           return _repository.DeleteTaks(id);
        }
    }
}
