﻿using ApplicationCore.Interfaces;
using QuizNetCoreAPI.DAL.Entities;
using QuizNetCoreAPI.DAL.Interfaces;

namespace ApplicationCore
{
    /// <inheritdoc/>
    public class Authorizer : IAuthorize
    {
        private readonly IUserRepository _repository;

        public Authorizer(IUserRepository repository)
        {
            _repository = repository;
        }

        public User Authorize(string userName, string password)
        {
           return _repository.GetUser(userName, password);
        }
    }
}
